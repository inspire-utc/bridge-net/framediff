# Use the official Python base image
FROM python:3.9-slim

# Set the working directory to /app
WORKDIR /app

# Copy the requirements.txt file into the container
COPY requirements.txt .

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Copy the rest of the application code into the container
COPY . .

# Set the working directory to /app/framediff
WORKDIR /app/framediff

# Make the necessary directories for the application
RUN mkdir -p uploads extracted_frames zipped_frames

# Make port 5000 available to the world outside the container
EXPOSE 5000

# Define the environment variable for Flask
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

# Run the command to start the Flask application
CMD ["flask", "run"]
