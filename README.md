# framediff

A tool that diffs frames of a video, and selects frames where a lot of movement occurs. The tool intelligently selects frames for annotation.


## Endpoint: `/upload`

### Method: `POST`

This endpoint is used to upload a file and process it.

#### Request

The request should contain a multipart/form-data where the file to be uploaded is attached with the key 'file'. The file should be of an allowed type as specified by the server.

#### Response

Upon successful upload and processing of the file, a JSON object is returned

```

```

If the file type is not allowed, the user will receive a response with the status code 400, and the message "File type not allowed".

#### Example Request

```bash
curl -X POST -F "file=@/path/to/your/file" http://yourserver.com/upload
```

---

## Endpoint: `/status/<process_id>`

### Method: `GET`

This endpoint provides the status of a running job associated with a unique process ID.

#### Parameters

- `process_id` (path): The unique identifier of the processing job. This ID is returned upon successful file upload and processing initiation.

#### Response

- If the processing job is still running, the server will return a JSON object containing the status of the processing job.
- If the processing job has completed, the user is redirected to the download page for the completed job.
- If an invalid process ID is provided, the server will return a 404 status with the message "Invalid process ID."

#### Example Request

```bash
curl -X GET http://yourserver.com/status/<process_id>
```

#### Example Response 
While processing:

```json
{
    "frames": {
        "frame": 80,
        "total": 100
    },
    "selected_frames": []
}
```

If the process is still running, the `selected_frames` object is empty

Finished:
```json
{
    "frames": {
        "frame": 80,
        "total": 100
    },
    "selected_frames": [
        {
            "frame_no": 10,
            "url": "/extracted_frames/<process_id>/<image_name>"
        }
    ]
}

Once the job is finished, the selected_frames property is populated

```

```bash
http://yourserver.com/download_zip?process_id=<unique_process_id>
```

## Endpoint: /download/<process_id>

### Method: GET

This endpoint allows the user to download the processed files associated with a unique process ID.

#### Parameters

- `process_id` (path): The unique identifier of the processing job. This ID is returned upon successful file upload and processing initiation.

#### Response

- If the processing job has completed and the processed files are ready, the server will provide the processed files as a .zip download.
- If an invalid process ID is provided, or the processing job has not yet completed, the server will return a 404 status with the message "Invalid process ID or the process has not completed yet."

#### Example Request

```bash
curl -X GET http://yourserver.com/download/<process_id>
```