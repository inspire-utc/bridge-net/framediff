#!/bin/bash


# define parameters to be passed into the frame extractor
NTH_FRAME=10

# If the user wishes to override the defaults, place the values in framediff.env
if [ -f "$HOME/framediff.env" ]; then
    source $HOME/framediff.env
fi

python3 videodiff.py $1 --nth-frame=$NTH_FRAME | python3 selectframes.py $1 $2
