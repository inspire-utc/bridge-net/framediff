import os
import subprocess
import uuid
import shutil
from flask import Flask, request, redirect, url_for, render_template, send_from_directory, jsonify
from werkzeug.utils import secure_filename
import threading, time
import parse
import sys
UPLOAD_FOLDER = 'uploads'
EXTRACT_FOLDER = 'extracted_frames'
ZIP_FOLDER = 'zipped_frames'
ALLOWED_EXTENSIONS = {'mp4', 'avi', 'mkv', 'mov'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['EXTRACT_FOLDER'] = EXTRACT_FOLDER
app.config['ZIP_FOLDER'] = ZIP_FOLDER

processes = {}

# Check if the file extension is allowed
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Index route. This is primarily for debug purposes. Allows the developer to
# upload a video
@app.route('/', methods=['GET'])
def index():
    return render_template('upload.html')

# Upload route to post a file
@app.route('/upload', methods=['POST'])
def upload_file():
    file = request.files['file']
    # Check if user uploaded file and if the extension is allowed
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        # Upload the video to the specified directory
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(filepath)

        # Create a unique id for this job
        process_id = str(uuid.uuid4())
        
        # Create a unique output folder based on job ID
        output_folder = os.path.join(app.config['EXTRACT_FOLDER'], process_id)
        
        # Use the JOB ID as index for the subprocess
        status = processes[process_id] = {}
        
        proc = status["proc"] = subprocess.Popen(
            ["bash", "getframes.sh", filepath, output_folder],
            stderr=subprocess.PIPE,
        )

        status["status"] = {}

        def thread_worker():
            # So the idea here is to give the API Caller a general idea of the
            # progress of the frame diff process. We know how many frames are in
            # a video, and we know which frames just finished processing. This
            # is a rough estimate because the frames and diffed in parallel and
            # may arrive out of order.
    
            # This frame updates based on the last frame processed
            current_frame_no = 0
            while True:
                line = proc.stderr.readline()
                result = parse.parse("{}/{},{}", line.decode().strip())
                if result:
                    frame, total, diff = result
                    frame, total, diff = int(frame), int(total), int(diff)
                    stat = status["status"]
                    current_frame_no = max(current_frame_no, frame)
                    stat["frame"] = current_frame_no
                    # The total will never actually change
                    stat["total"] = total
                    
                
                if proc.poll() is not None:
                    print(f"{process_id} finished",file=sys.stderr)
                    return
                
                time.sleep(0.125)

        thrd = threading.Thread(target=thread_worker)
        thrd.start()

        return jsonify({ "job_id": process_id, })
    else:
        return "File type not allowed", 400

@app.route('/status/<process_id>', methods=['GET'])
def process_status(process_id):
    # Check if JOB ID exists
    if process_id in processes:
        selected_frames = []
        status = {
            "frames": processes[process_id]["status"],
            "selected_frames": selected_frames,
        }

        proc = processes[process_id]["proc"]
        if proc.poll() is not None:
            # Process finished running, which means we have the extracted frames

            extracted_dir = os.path.join(app.config['EXTRACT_FOLDER'], process_id)
            for file in os.listdir(extracted_dir):
                
                frame_no = parse.parse("{}_frame.jpg", file)
                assert frame_no
                data = {
                    "frame_no": int(frame_no[0]),
                    "url": os.path.join("/", extracted_dir, file),
                    "filename": file
                }
                selected_frames.append(data)
        return jsonify(status)
    else:
        return "Invalid process ID.", 404


@app.route("/extracted_frames/<process_id>/<image>", methods = ["GET"])
def extracted_frames(process_id, image):
    image_dir = os.path.join(app.config['EXTRACT_FOLDER'], process_id)
    return send_from_directory(image_dir, image)


# This route handles downloads. If the process is done and the frames were
# extracted, download the zip file
@app.route('/download_zip/<process_id>', methods=['GET'])
def download_zip(process_id):
    # Get the directory of the Job ID
    extracted_dir = os.path.join(app.config['EXTRACT_FOLDER'], process_id)
    if os.path.exists(extracted_dir):
        zip_filename = f"{process_id}.zip"
        zip_path = os.path.join(app.config['ZIP_FOLDER'], zip_filename)

        if not os.path.exists(app.config['ZIP_FOLDER']):
            os.makedirs(app.config['ZIP_FOLDER'])

        with open(zip_path, 'wb') as f_out:
            shutil.make_archive(os.path.splitext(zip_path)[0], 'zip', extracted_dir)

        return send_from_directory(app.config['ZIP_FOLDER'], zip_filename, as_attachment=True)
    else:
        return "Invalid process ID or the process has not completed yet.", 404

if __name__ == '__main__':
    for folder in [UPLOAD_FOLDER, EXTRACT_FOLDER, ZIP_FOLDER]:
        if not os.path.exists(folder):
            os.makedirs(folder)
    app.run(debug=True)