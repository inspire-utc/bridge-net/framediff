"""
USAGE: videodiff <video-path> [options]

Options
    --nth-frame=<int: default=10>       Diff every nth frame
    --ncores=<default: system count>    Split workload across ncores

Description:
    Use phase correlation algorithm to obtain a scalar metric of how each frame
    differs from the last frame. The output is printed to stdout in the csv format below
    <frame no>,<frame diff>
"""

import docopt
import sys
import cv2
import numpy as np
import csv
import os
import multiprocessing as mp
import time

# Function to calculate phase correlation between two numpy arrays (images)
def phase_correlation(a, b):
    G_a = np.fft.fft2(a)
    G_b = np.fft.fft2(b)
    conj_b = np.ma.conjugate(G_b)
    R = G_a * conj_b
    R /= np.absolute(R)
    r = np.fft.ifft2(R).real
    return np.argmax(r)


def main():
    # Parse command-line arguments using docopt
    args = docopt.docopt(__doc__)
    video_path = args["<video-path>"]
    nth_frame = int(args["--nth-frame"]) if args["--nth-frame"] else 10
    n_cores = int(args["--ncores"]) if args["--ncores"] else mp.cpu_count()

    # Open the video file using OpenCV
    capture = cv2.VideoCapture(video_path)

    # Initialize the frame counter
    ith_frame = 0

    # List to store all the frames
    all_frames = []

    # Loop through the video frames
    while True:
        # Read the next frame from the video
        success, frame = capture.read()

        # Save the frame and its number if the frame is successfully read and the frame number is a multiple of nth_frame
        if success and ith_frame % nth_frame == 0:
            all_frames.append((ith_frame, frame))

        # Break the loop if the end of the video is reached
        elif not success:
            print(f"Read {ith_frame} frames", file=sys.stderr)
            break

        # Increment the frame counter
        ith_frame += 1

    # Create two lists of frames for pairwise comparison
    frame1 = all_frames[: len(all_frames) - 1]
    frame2 = all_frames[1:]

    # Initialize csv writers for stdout and stderr
    writer = csv.writer(sys.stdout)
    debug = csv.writer(sys.stderr)

    
    for i, ((frame_no, f1), (_, f2)) in enumerate(zip(frame1, frame2)):
        # Check if the current index is assigned to the current core
        
        result = phase_correlation(f1, f2)

        # Write the result to stdout and stderr
        writer.writerow([frame_no, result])
        debug.writerow([f"{frame_no}/{ith_frame}", result])

   

    # So I tried using os.waitpid() to wait for all processes to stop, didn't work
    # Also tried using multiprocessing.Pool to create a pool of workers, but
    # that didn't work either
    # But using a semaphore as a counter did the trick

   
    print("DONE")

if __name__ == "__main__":
    main()
