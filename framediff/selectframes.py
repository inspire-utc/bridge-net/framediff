"""
USAGE: selectframes <video-path> <output-dir>

Description:
    This script takes a video file path and an output directory as input, then reads
    frame numbers and their corresponding phase values from standard input (stdin). 
    It calculates the phase correlation changes between consecutive frames and selects 
    the frames with a change higher than the average. 
    The selected frames are then saved as images in the output directory.
"""

import docopt
import cv2
import csv
import sys
import os


def main():

    # Parse command-line arguments using docopt
    args = docopt.docopt(__doc__)
    videopath = args["<video-path>"]
    outputdir = args["<output-dir>"]

    # Check if output directory exists, if it does, exit with an error message
    if os.path.exists(outputdir):
        print(f"{outputdir} exists", file=sys.stderr)
        exit(1)

    # Read frame_no, phase from stdin and store them as tuples in a list
    phases = []

    try:
        for frame, phase in csv.reader(sys.stdin):
            phases.append((int(frame), int(phase)))
    except:
        pass

    # Sort the phases list by frame number
    phases = sorted(phases, key=lambda x: x[0])

    # Calculate phase correlation change between consecutive frames
    phases1 = phases[: len(phases) - 1]
    phases2 = phases[1:]
    derivatives = [
        (phases[i][0], abs(phases2[i][1] - phases1[i][1])) for i in range(len(phases1))
    ]

    # Calculate the average of the phase correlation changes
    avg = sum(der[1] for der in derivatives) / len(derivatives)

    # Select the frames with phase correlation change higher than the average
    frames = set(der[0] for der in derivatives if der[1] > avg)

    # Initialize the frame counter
    ith_frame = 0

    # Open the video file using OpenCV
    capture = cv2.VideoCapture(videopath)

    # Create the output directory if it doesn't exist
    if not os.path.exists(outputdir):
        os.mkdir(outputdir)

    # Loop through the video frames
    while True:
        # Read the next frame from the video
        success, frame = capture.read()

        # Break the loop if the end of the video is reached
        if not success:
            print(f"Read {ith_frame} frames", file=sys.stderr)
            break

        # Save the frame to the output directory if its frame number is in the selected frames set
        if ith_frame in frames:
            cv2.imwrite(f"{outputdir}/{ith_frame}_frame.jpg", frame)

        # Increment the frame counter
        ith_frame += 1


if __name__ == "__main__":
    main()
